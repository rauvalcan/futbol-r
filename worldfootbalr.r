# futbol - r

library(worldfootballR)

team_urls <- tm_league_team_urls(country_name = "England", start_year = 2020)
# if it's not a league in the stored leagues data in worldfootballR_data repo:
league_one_teams <- tm_league_team_urls(start_year = 2020, league_url = "https://www.transfermarkt.com/league-one/startseite/wettbewerb/GB3")


tm_team_player_urls(team_url = "https://www.transfermarkt.com/fc-burnley/startseite/verein/1132/saison_id/2020")

# for one team:
bayern <- tm_team_transfers(team_url = "https://www.transfermarkt.com/fc-bayern-munchen/startseite/verein/27/saison_id/2020")

# or for multiple teams:
team_urls <- tm_league_team_urls(country_name = "England", start_year = 2020)
epl_xfers_2020 <- tm_team_transfers(team_url = team_urls)


# for one team:
bayern <- tm_squad_stats(team_url = "https://www.transfermarkt.com/fc-bayern-munchen/startseite/verein/27/saison_id/2020")

# or for multiple teams:
team_urls <- tm_league_team_urls(country_name = "England", start_year = 2020)
epl_team_players_2020 <- tm_squad_stats(team_url = team_urls)


get_match_summary(match_url)



